---
title: 'Cloud Computing Fundamentals'
---

## Cloud Environments

### Public Cloud
Compute resources are provided as a service to a very large customer group in order to enable elastic use of a static resource pool.

A provider offering Compute resources according to IaaS, PaaS, or SaaS has to maintain physical data centers. Compute resources, nevertheless, shall be made accessible dynamically.

The hosting environment is shared between many customers possibly reducing the costs for an individual customer. Leveraging economies of scale enables a dynamic use of resources, because workload peaks of some customers occur during times of low workload of other customers.

### Private Cloud
Computing resources are provided as a service exclusively to one customer in order to meet high requirements on privacy, security, and trust while enabling elastic use of a static resource pool as good as possible.

Many factors, such as legal limitations, trust, and security regulations, motivate dedicated, company-internal hosting environments only accessible by employees and applications of a single company.

Cloud computing properties are enabled in a company-internal data center. Alternatively, the Private Cloud may be hosted exclusively in the data center of an external provider, then referred to as outsourced Private Cloud. Sometimes, Public Cloud providers also offer means to create an isolated portion of their cloud made accessible to only one customer: a virtual Private Cloud.


### Community Cloud
Computing resources are provided as a service to a group of customers trusting each other in order to enable collaborative elastic use of a static resource pool.

Whenever companies collaborate, they commonly have to access shared applications and data to do business. While these companies trust each other due to established contracts etc., the handled data and application functionality may be very sensitive and critical to their business.

Compute resources required by all collaborating partners are offered in a controlled environment accessible only by the community of companies that generally trust each other.

### Hybrid Cloud
Different clouds and static data centers are integrated to form a homogeneous hosting environment.

A company is likely to use a large set of applications to support its business, which have versatile requirements making different Cloud Deployment Models suitable to host them.

Private Clouds, Public Clouds, Community Clouds, and static data centers are integrated to deployed applications to the hosting environment best suited for their requirements while interconnection of these environments.


## Cloud Computing Stacks

### Infrastructure as a Service (IaaS)
Providers share physical and virtual hardware Compute resources between customers to enable self-service, rapid elasticity, and pay-per-use pricing.

In the scope of Periodic Workloads with reoccurring peaks and the special case of 'One-Time Workloads' with one dramatic increase in workload, Compute resources have to be provisioned flexibly.

A provider offers physical and virtual hardware, such as servers, storage and networking infrastructure that can be provisioned and decommissioned quickly through a self-service interface.

### Platform as a Service (PaaS)
Providers share Compute resources providing an application hosting environment between customers to enable self-service, rapid elasticity, and pay-per-use pricing.

If many customers require similar hosting environments for their applications, there are many redundant installations resulting in an inefficient use of the overall cloud.

A cloud provider offers managed operating systems and middleware. Management operations are handled by the provider, such as the elastic scaling and failure resiliency of hosted applications.

### Software as a Service (SaaS)
Providers share Compute resources providing human-usable application software between customers to enable self-service, rapid elasticity, and pay-per-use pricing.

Small and medium enterprises may not have the manpower and know-how to develop custom software applications. 
Other applications have become commodity and are used by many companies, for example, office suites, collaboration software, or communications software.

A provider offers a complete software application to customers who may use it on-demand and via a self-service interface.


## Workloads

### Static
Computing resources with an equal utilization over time experience Static Workload.

Static Workloads are characterized by a more-or-less flat utilization profile over time within certain boundaries.

Application experiencing Static Workload is less likely to benefit from an elastic cloud that offers a pay-per-use billing, because the number of required resources is constant.

### Periodic
Computing resources with peak utilization at reoccurring time intervals experience periodic workload.

Periodic tasks and routines are common. For example, monthly paychecks, monthly telephone bills, yearly car checkups, weekly status reports, or the daily use of public transport during rush hour, all these tasks and routines occur in well-defined intervals.

The cost-saving potential in scope of Periodic Workload is to use a provider with a pay-per-use pricing model allowing the decommissioning of resources during non-peak times.

### One-Time
Computing resources with an equal utilization over time disturbed by a strong peak occurring only once experience One-Time Workload.

As a special case of Periodic Workload, the peaks of periodic utilization can occur only once in a very long timeframe. Often, this peak is known in advance as it correlates to a certain event or task.

The elasticity of a cloud is used to obtain computing resources necessary. The provisioning and decommissioning of computing resources can often be realized as a manual task, because it is performed at a known point in time.

### Unpredictable
Computing resources with a random and unforeseeable utilization over time experience unpredictable workload.

Random workloads are a generalization of Periodic Workloads as they require elasticity but are not predictable. Such workloads occur quite often in the real world.

Unplanned provisioning and decommissioning of computing resources is required. The necessary provisioning and decommissioning of computing resources is, therefore, automated to align the resource numbers to changing workload.

### Continuously Changing
Computing resources with a utilization that grows or shrinks constantly over time experience Continuously Changing Workload.

Many applications experience a long term change in workload.

Continuously Changing Workload is characterized by an ongoing continuous growth or decline of the utilization. 
Elasticity of clouds enables applications experiencing Continuously Changing Workload to provision or decommission resources with the same rate as the workload changes.



