---
title: 'Cloud Patterns'
menu:
  main:
    name:  'Cloud Patterns'   
    identifier: 'cloud-patterns'
    weight: 1
---

Design patterns that capture and modularize technology-centric solutions distinct or relevant to modern-day cloud computing platforms and business-centric cloud technology architectures. Part of this catalog is comprised of compound patterns that tackle contemporary cloud delivery and deployment models (such as public cloud, IaaS, etc.) and decompose them into sets of co-existent patterns that establish core and optional feature sets provided by these environments.


Cloud patterns are applied via the implementation of individual or combinations of different technology mechanisms. Together, the documentation of patterns and mechanisms provides an extremely concrete view of cloud architecture layers and the individual building blocks that represent the moving parts that can be assembled in creative ways to leverage cloud environments for business automation.

## [Cloud Computing Pattern List](../CCPatternList)
List of Cloud Computing Patterns.

## [Cloud Computing Fundamentals](../CloudComputingFundamentals) 
Describe cloud service models and cloud deployment types analogous to the [NIST](../CloudComputing) cloud definition. These patterns extend this definition by covering the conditions under which a certain service model and deployment type should be used for a cloud application.

## [Cloud Offerings](../CloudOfferings)
Describe the functionality offered by cloud providers to be used by an application for processing of workload, communication, and data storage. Again, these patterns cover the conditions under which an offering should be selected, as well as the implications on the application.

## [Cloud Application Architectures](../CloudApplicationArchitectures)
Describe the general structure of the cloud application and specific application components for user interfaces, processing, and data handling. 

## [Cloud Application Management](../CloudApplicationManagement)
Describes how these applications can be managed during runtime using additional management components, which rely on functionality provided by the application itself, cloud offerings, and the cloud environment.

## [Composite Cloud Applications](../CompositeCloudApplications)
Cover frequent combinations of patterns from all other categories in various use cases.
